# Creating a JSON Patch endpoint in Go

Sample code to go alongside [a blog post](https://www.jvt.me/posts/2022/11/09/http-json-patch-go/).

Licensed under Apache-2.0
