package main

import (
	"encoding/json"
	"io"
	"log"
	"net/http"

	jsonpatch "github.com/evanphx/json-patch"
)

// Account is the domain model for i.e. a user account, and has an immutable ID, and an email field
type Account struct {
	ID    string
	Email string
}

// AccountRepresenation is the RESTful representation of the Account type
type AccountRepresenation struct {
	// not allowed to update
	ID    string `json:"id"`
	Email string `json:"email"`
}

func AccountsPatchHandler(w http.ResponseWriter, r *http.Request) {
	// we'd also want to check it's actually an application/json-patch+json
	if r.Method != "PATCH" {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	body, err := io.ReadAll(r.Body)
	if err != nil {
		w.Write([]byte(err.Error()))
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	patch, err := jsonpatch.DecodePatch(body)
	if err != nil {
		w.Write([]byte(err.Error()))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// this would be retrieved by i.e. a Service or Repository tier object
	domainModel := Account{
		ID:    "1",
		Email: "example@example.com",
	}

	// then we map it into the type that's expected by our HTTP consumers
	current := AccountRepresenation{
		ID:    domainModel.ID,
		Email: domainModel.Email,
	}

	currentBytes, err := json.Marshal(current)
	if err != nil {
		w.Write([]byte(err.Error()))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	modifiedBytes, err := patch.Apply(currentBytes)
	if err != nil {
		w.Write([]byte(err.Error()))
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	var modified AccountRepresenation
	err = json.Unmarshal(modifiedBytes, &modified)
	if err != nil {
		w.Write([]byte(err.Error()))
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	// perform business logic checks
	if modified.ID != current.ID {
		w.WriteHeader(http.StatusUnprocessableEntity)
		w.Write([]byte("The ID field cannot be modified"))
		return
	}

	// we'd then usually persist the data here
	// but in this example, we don't have a way to

	// and we need to make sure that when returning the re-marshalled model, as if we return `modifiedBytes`, we could end up with unsupported fields being returned
	outBytes, err := json.Marshal(modified)
	_, _ = w.Write(outBytes)
}

func main() {
	// this isn't an ideal way of doing an ID patch, but for this contrived example, is sufficient
	http.Handle("/accounts/1", http.HandlerFunc(AccountsPatchHandler))

	port := "8000"

	srv := &http.Server{
		Addr: "0.0.0.0:" + port,
	}
	log.Println("Listening on port", port)
	log.Fatal(srv.ListenAndServe())
}
