module gitlab.com/tanna.dev/go-jsonpatch-http

go 1.19

require github.com/evanphx/json-patch v0.5.2

require github.com/pkg/errors v0.9.1 // indirect
